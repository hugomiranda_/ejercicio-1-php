<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 4</title>
    <style>
        h1 { 
            text-align: center;
        }

        table {
            border-collapse: collapse;
            width: 50%;
            margin: 20px auto;
        }

        th, td {
            border: 1px solid #000;
            padding: 8px;
            text-align: center;
        }

        th {
            background-color: #A7A7A7;
        }

        .encabezado1 {
            background-color: yellow;
        }
    </style>
</head>
<body>
    <h1>Stock de Productos</h1>
    <?php
    $encabezadoProductos = "Productos";
    $encabezados = array("Nombre", "Cantidad", "Precio (Gs)");
    $datos = array(
        array("Coca Cola", "100", "4.500"),
        array("Pepsi", "30", "4.800"),
        array("Sprite", "20", "4.500"),
        array("Guaraná", "200", "4.500"),
        array("SevenUp", "24", "4.800"),
        array("Mirinda Naranja", "56", "4.800"),
        array("Mirinda Guaraná", "89", "4.800"),
        array("Fanta Naranja", "10", "4.500"),
        array("Fanta Piña", "2", "4.500")
    );

    echo '<table>';
    echo '<tr><th class="encabezado1" colspan="3">' . $encabezadoProductos . '</th></tr>';
    echo '<tr>';
    foreach ($encabezados as $encabezado) {
        echo '<th>' . $encabezado . '</th>';
    }
    echo '</tr>';

    foreach ($datos as $fila) {
        echo '<tr>';
        foreach ($fila as $valor) {
            echo '<td>' . $valor . '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
    ?>
</body>
</html>
